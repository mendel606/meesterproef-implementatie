﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Logic;

namespace Factory
{
    public static class DataClasses
    {
		public static IOrdeDataModifier ordeDataModifier()
		{
			return new OrdeDA();
		}

		public static IUitslagDataModifier uitslagDataModifier()
		{
			return new UitslagDA();
		}

		public static ICoalitieDataModifier coalitieDataModifier()
		{
			return new CoalitieDA();
		}

		public static IVerkiezingDataModifier verkiezingDataModifier()
		{
			return new VerkiezingDA();
		}
	}
}
