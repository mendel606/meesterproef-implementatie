﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;

namespace DAL
{
	public class VerkiezingDA : IVerkiezingDataModifier
	{
		public string connectionstring = "Server=mssql.fhict.local;Database=dbi383964;User Id=dbi383964;Password=Schaapje11";
		public SqlConnection connection;

		public VerkiezingDA()
		{
			try
			{
				using (connection = new SqlConnection(connectionstring))
				{
					connection.Open();
				}
			}
			catch { }
		}

		public void VoegToe(Verkiezing verkiezing)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"INSERT INTO Verkiezing(Naam, Datum, AantalStoelen) VALUES(@Naam, @Datum, @AantalStoelen)", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Naam", verkiezing.Naam);
				commend.Parameters.AddWithValue("@Datum", verkiezing.Datum);
				commend.Parameters.AddWithValue("@AantalStoelen", verkiezing.AantalStoelen);

				commend.ExecuteNonQuery();
			}
		}

		public void Verander(Verkiezing verkiezing)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"UPDATE Verkiezing SET Naam = @Naam, Datum = @Datum, AantalStoelen = @AantalStoelen, Coalitie = @Coalitie WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Naam", verkiezing.Id);
				commend.Parameters.AddWithValue("@Naam", verkiezing.Naam);
				commend.Parameters.AddWithValue("@Datum", verkiezing.Datum);
				commend.Parameters.AddWithValue("@AantalStoelen", verkiezing.AantalStoelen);
				commend.Parameters.AddWithValue("@Coalitie", verkiezing.Coalitie.Id);

				commend.ExecuteNonQuery();
			}
		}

		public void Verwijder(int verkiezingId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"DELETE * FROM Verkiezing WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", verkiezingId);

				commend.ExecuteNonQuery();
			}
		}

		public Verkiezing Verkiezing(int verkiezingId)
		{
			CoalitieDA coalitieDA = new CoalitieDA();
			UitslagDA uitslagDA = new UitslagDA();

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Verkiezing WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", verkiezingId);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						return new Verkiezing
						{
							Id = Convert.ToInt32(reader["Id"]),
							Naam = reader["Naam"].ToString(),
							AantalStoelen = Convert.ToInt32(reader["AantalStoelen"]),
							Coalitie = coalitieDA.Coalitie(Convert.ToInt32(reader["Coalitie"])),
							Datum = Convert.ToDateTime(reader["Datum"]),
							Uitslagen = uitslagDA.Uitslagen(Convert.ToInt32(reader["Id"]))
						};
					}
				}
			}
			return null;
		}

		public List<Verkiezing> Verkiezingen()
		{
			CoalitieDA coalitieDA = new CoalitieDA();
			UitslagDA uitslagDA = new UitslagDA();
			List<Verkiezing> verkiezingen = new List<Verkiezing>();

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Verkiezing", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();
				

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						Verkiezing verkiezing = new Verkiezing
						{
							Id = Convert.ToInt32(reader["Id"]),
							Naam = reader["Naam"].ToString(),
							AantalStoelen = Convert.ToInt32(reader["AantalStoelen"]),
							Datum = Convert.ToDateTime(reader["Datum"]),
							Uitslagen = uitslagDA.Uitslagen(Convert.ToInt32(reader["Id"]))
						};
						try
						{
							verkiezing.Coalitie = coalitieDA.Coalitie(Convert.ToInt32(reader["Coalitie"]));
						}
						catch { }
						verkiezingen.Add(verkiezing);
					}
				}
			}
			return verkiezingen;
		}
	}
}
