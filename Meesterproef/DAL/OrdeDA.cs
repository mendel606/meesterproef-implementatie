﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;

namespace DAL
{
	public class OrdeDA : IOrdeDataModifier
	{
		public string connectionstring = "Server=mssql.fhict.local;Database=dbi383964;User Id=dbi383964;Password=Schaapje11";
		public SqlConnection connection;

		public OrdeDA()
		{
			try
			{
				using (connection = new SqlConnection(connectionstring))
				{
					connection.Open();
				}
			}
			catch
			{
				
			}
		}

		public void VoegToe(Orde orde)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"INSERT INTO Orde(Afkorting, Naam, OrdeTrekker) VALUES(@Afkorting, @Naam, @OrdeTrekker)", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Afkorting", orde.Afkorting);
				commend.Parameters.AddWithValue("@Naam", orde.Naam);
				commend.Parameters.AddWithValue("@OrdeTrekker", orde.OrdeTrekker);

				commend.ExecuteNonQuery();
			}
		}

		public void Verander(Orde orde)
		{
			if (orde.Naam.Length > 0 && orde.Afkorting.Length > 0 && orde.OrdeTrekker.Length > 0)
			{
				using (connection = new SqlConnection(connectionstring))
				using (SqlCommand commend = new SqlCommand(
					"UPDATE Orde SET Afkorting = @Afkorting, Naam = @Naam, OrdeTrekker = @OrdeTrekker WHERE Id = @Id", connection))
				using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
				{
					connection.Open();

					commend.Parameters.AddWithValue("@Id", orde.Id);
					commend.Parameters.AddWithValue("@Afkorting", orde.Afkorting);
					commend.Parameters.AddWithValue("@Naam", orde.Naam);
					commend.Parameters.AddWithValue("@OrdeTrekker", orde.OrdeTrekker);

					commend.ExecuteNonQuery();
				}
			}
		}

		public Orde Orde(int ordeId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Orde WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", ordeId);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						return new Orde
						{
							Id = Convert.ToInt32(reader["Id"]),
							Naam = reader["Naam"].ToString(),
							Afkorting = reader["Afkorting"].ToString(),
							OrdeTrekker = reader["OrdeTrekker"].ToString()
						};
					}
				}
			}
			return null;
		}

		public List<Orde> Ordes()
		{
			List<Orde> ordes = new List<Orde>();
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Orde", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						ordes.Add(new Orde
						{
							Id = Convert.ToInt32(reader["Id"]),
							Naam = reader["Naam"].ToString(),
							Afkorting = reader["Afkorting"].ToString(),
							OrdeTrekker = reader["OrdeTrekker"].ToString()
						});
					}
				}
			}
			return ordes;
		}

		public void Verwijder(int ordeId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"DELETE * FROM Orde WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", ordeId);

				commend.ExecuteNonQuery();
			}
		}
	}
}
