﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;

namespace DAL
{
	public class CoalitieDA : ICoalitieDataModifier
	{
		public string connectionstring = "Server=mssql.fhict.local;Database=dbi383964;User Id=dbi383964;Password=Schaapje11";
		public SqlConnection connection;

		public void VoegToe(Coalitie coalitie)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"INSERT INTO Coalitie(Naam, OpperRidder) VALUES(@Naam, @OpperRidder)", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Naam", coalitie.Naam);
				commend.Parameters.AddWithValue("@OpperRidder", coalitie.OpperRidder);

				commend.ExecuteNonQuery();
			}

			VoegOrdesToe(coalitie);
		}

		private void VoegOrdesToe(Coalitie coalitie)
		{
			foreach(Orde orde in coalitie.Ordes)
			{
				using (connection = new SqlConnection(connectionstring))
				using (SqlCommand commend = new SqlCommand(
					"INSERT INTO Coalitie_Orde(Coalitie, Orde) VALUES(@Coalitie, @Orde)", connection))
				using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
				{
					connection.Open();

					commend.Parameters.AddWithValue("@Coalitie", coalitie.Id);
					commend.Parameters.AddWithValue("@Orde", orde.Id);

					commend.ExecuteNonQuery();
				}
			}
		}

		public void Verander(Coalitie coalitie)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"UPDATE Coalitie c SET c.Naam = @Naam, c.OpperRidder = @OpperRidder WHERE c.Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", coalitie.Id);
				commend.Parameters.AddWithValue("@Naam", coalitie.Naam);
				commend.Parameters.AddWithValue("@OpperRidder", coalitie.OpperRidder);

				commend.ExecuteNonQuery();
			}
		}
		
		public Coalitie Coalitie(int coalitieId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Coalitie WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", coalitieId);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						return new Coalitie()
						{
							Id = Convert.ToInt32(reader["Id"]),
							Naam = reader["Naam"].ToString(),
							OpperRidder = reader["OpperRidder"].ToString(),
							Ordes = Ordes(Convert.ToInt32(reader["Id"]))
						};
					}
				}

				commend.ExecuteNonQuery();
			}
			return null;
		}

		public List<Coalitie> Coalities()
		{
			List<Coalitie> coalities = new List<Coalitie>();
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Coalitie", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while(reader.Read())
					{
						coalities.Add(new Coalitie()
						{
							Id = Convert.ToInt32(reader["Id"]),
							Naam = reader["Naam"].ToString(),
							OpperRidder = reader["OpperRidder"].ToString(),
							Ordes = Ordes(Convert.ToInt32(reader["Id"]))
						});
					}
				}
			}
			return coalities;
		}

		private List<Orde> Ordes(int coalitieId)
		{
			List<int> OrdeIds = new List<int>();
			OrdeDA ordeDA = new OrdeDA();
			List<Orde> ordes = new List<Orde>();
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT Orde FROM Coalitie_Orde WHERE Coalitie = @Coalitie", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Coalitie", coalitieId);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						OrdeIds.Add(Convert.ToInt32(reader["Orde"]));
					}
				}
			}
			
			foreach(int ordeId in OrdeIds)
			{
				ordes.Add(ordeDA.Orde(ordeId));
			}

			return ordes;
		}

		public void Verwijder(int coalitieId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"DELETE * FROM Coalitie WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", coalitieId);

				commend.ExecuteNonQuery();
			}
		}
	}
}
