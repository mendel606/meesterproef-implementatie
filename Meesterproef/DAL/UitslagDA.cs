﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;

namespace DAL
{
	public class UitslagDA : IUitslagDataModifier
	{
		public string connectionstring = "Server=mssql.fhict.local;Database=dbi383964;User Id=dbi383964;Password=Schaapje11";
		public SqlConnection connection;

		public void VoegToe(Uitslag uitslag, int verkiezingId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"INSERT INTO Uitslag(Verkiezing, Orde, Stemmen) VALUES(@Verkiezing, @Orde, @Stemmen)", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Verkiezing", verkiezingId);
				commend.Parameters.AddWithValue("@Orde", uitslag.Orde.Id);
				commend.Parameters.AddWithValue("@Stemmen", uitslag.Stemmen);

				commend.ExecuteNonQuery();
			}
		}

		public void Verander(Uitslag uitslag)
		{
			if(uitslag.Stemmen > 0)
			{
				using (connection = new SqlConnection(connectionstring))
				using (SqlCommand commend = new SqlCommand(
					"UPDATE Uitslag SET Stemmen = @Stemmen, Orde = @Orde WHERE Id = @Id", connection))
				using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
				{
					connection.Open();
					commend.Parameters.AddWithValue("@Id", uitslag.Id);
					commend.Parameters.AddWithValue("@Stemmen", uitslag.Stemmen);
					commend.Parameters.AddWithValue("@Orde", uitslag.Orde.Id);

					commend.ExecuteNonQuery();
				}
			}
		}

		public void Verwijder(int uitslagId)
		{
			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"DELETE * FROM Uitslag WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", uitslagId);

				commend.ExecuteNonQuery();
			}
		}

		public Uitslag Uitslag(int uitslagId)
		{
			OrdeDA ordeDA = new OrdeDA();

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Uitslag WHERE Id = @Id", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Id", uitslagId);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						Uitslag uitslag = new Uitslag()
						{
							Id = Convert.ToInt32(reader["Id"]),
							Stemmen = Convert.ToInt32(reader["Stemmen"])
						};
						try
						{
							uitslag.Orde = ordeDA.Orde(Convert.ToInt32(reader["Orde"]));
						}
						catch
						{
							uitslag.Orde = null;
						}
						return uitslag;
					}
				}
			}
			return null;
		}

		public List<Uitslag> Uitslagen(int verkiezingId)
		{
			OrdeDA ordeDA = new OrdeDA();
			List<Uitslag> uitslagen = new List<Uitslag>();

			using (connection = new SqlConnection(connectionstring))
			using (SqlCommand commend = new SqlCommand(
				"SELECT * FROM Uitslag WHERE Verkiezing = @Verkiezing", connection))
			using (SqlDataAdapter adaptor = new SqlDataAdapter(commend))
			{
				connection.Open();

				commend.Parameters.AddWithValue("@Verkiezing", verkiezingId);

				SqlDataReader reader = commend.ExecuteReader();

				if (reader.HasRows)
				{
					while (reader.Read())
					{
						uitslagen.Add(new Uitslag
						{
							Id = Convert.ToInt32(reader["Id"]),
							Orde = ordeDA.Orde(Convert.ToInt32(reader["Orde"])),
							Stemmen = Convert.ToInt32(reader["Stemmen"])
						});
					}
				}
			}
			return uitslagen;
		}
	}
}
