﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Logic.Tests
{
	[TestClass()]
	public class VerkiezingTests
	{
		[TestMethod()]
		public void ExportTest()
		{
			//Arange
			List<Orde> ordes = new List<Orde>()
				{
					new Orde() { Id = 0, Afkorting = "VVR" },
					new Orde(){ Id = 1, Afkorting = "RVV" },
					new Orde(){ Id = 2, Afkorting = "CRA" },
					new Orde(){ Id = 3, Afkorting = "R65" }
				};
			Verkiezing verkiezing = new Verkiezing()
			{
				Id = 0,
				Datum = DateTime.Now,
				Naam = "Ebonië",
				AantalStoelen = 150
			};
			verkiezing.Uitslagen = new List<Uitslag>
			{
				new Uitslag()
				{
					Id = 0,
					Orde = new Orde(){ Id = 0, Afkorting = "VVR" },
					Stemmen = 2238351
				},
				new Uitslag()
				{
					Id = 1,
					Orde = new Orde(){ Id = 1, Afkorting = "RVV" },
					Stemmen = 1372941
				},
				new Uitslag()
				{
					Id = 2,
					Orde = new Orde(){ Id = 2, Afkorting = "CRA" },
					Stemmen = 1301796
				},
				new Uitslag()
				{
					Id = 3,
					Orde = new Orde(){ Id = 3, Afkorting = "R65" },
					Stemmen = 1285819
				},
				new Uitslag()
				{
					Id = 4,
					Orde = new Orde(){ Id = 4, Afkorting = "LR" },
					Stemmen = 959600
				},
				new Uitslag()
				{
					Id = 5,
					Orde = new Orde(){ Id = 5, Afkorting = "SR" },
					Stemmen = 955633
				},
				new Uitslag()
				{
					Id = 6,
					Orde = new Orde(){ Id = 6, Afkorting = "DRO" },
					Stemmen = 599699
				},
				new Uitslag()
				{
					Id = 7,
					Orde = new Orde(){ Id = 7, Afkorting = "RU" },
					Stemmen = 356271
				},
				new Uitslag()
				{
					Id = 8,
					Orde = new Orde(){ Id = 8, Afkorting = "DVR" },
					Stemmen = 335214
				},
				new Uitslag()
				{
					Id = 9,
					Orde = new Orde(){ Id = 9, Afkorting = "R50+" },
					Stemmen = 327121
				},
				new Uitslag()
				{
					Id = 10,
					Orde = new Orde(){ Id = 10, Afkorting = "GRB" },
					Stemmen = 218950
				},
				new Uitslag()
				{
					Id = 11,
					Orde = new Orde(){ Id = 11, Afkorting = "SLR" },
					Stemmen = 216147
				},
				new Uitslag()
				{
					Id = 12,
					Orde = new Orde(){ Id = 12, Afkorting = "FvR" },
					Stemmen = 187162
				}
			};
			verkiezing.Coalitie = new Coalitie()
			{
				Id = 0,
				Naam = "Coalitie 1",
				OpperRidder = "Frans Peters",
				Ordes = ordes
			};
			string fileName = @"C:\Mendel\" + verkiezing.Naam + " " + verkiezing.Datum.ToShortDateString() + ".txt";
			//Act
			verkiezing.Export();

			//Assert
			Assert.IsTrue(File.Exists(fileName));
		}

		[TestMethod()]
		public void TafelMeerderheidTest_PositiveMeerderheid()
		{
			//Arange
			Verkiezing verkiezing = new Verkiezing()
			{
				Naam = "Nederland",
				Datum = DateTime.Now,
				AantalStoelen = 100,
				Uitslagen = new List<Uitslag>()
				{
					new Uitslag()
					{
						Id = 0,
						Orde = new Orde(){ Id = 0 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 1,
						Orde = new Orde(){ Id = 1 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 2,
						Orde = new Orde(){ Id = 2 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 3,
						Orde = new Orde(){ Id = 3 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 4,
						Orde = new Orde(){ Id = 4 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 5,
						Orde = new Orde(){ Id = 5 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 6,
						Orde = new Orde(){ Id = 6 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 7,
						Orde = new Orde(){ Id = 7 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 8,
						Orde = new Orde(){ Id = 8 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 9,
						Orde = new Orde(){ Id = 9 },
						Stemmen = 200
					}
				}
			};
			bool expected = true;
			List<Orde> ordes = new List<Orde>() { new Orde() { Id = 1 }, new Orde() { Id = 2 }, new Orde() { Id = 3 }, new Orde() { Id = 4 }, new Orde() { Id = 5 }, new Orde() { Id = 6 } };

			//Act
			bool? actual = verkiezing.TafelMeerderheid(ordes);

			//Assert
			Assert.AreEqual(expected, actual);
		}

		[TestMethod()]
		public void TafelMeerderheidTest_PositiveMinderheid()
		{
			//Arange
			Verkiezing verkiezing = new Verkiezing()
			{
				Naam = "Nederland",
				Datum = DateTime.Now,
				AantalStoelen = 100,
				Uitslagen = new List<Uitslag>()
				{
					new Uitslag()
					{
						Id = 0,
						Orde = new Orde(){ Id = 0 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 1,
						Orde = new Orde(){ Id = 1 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 2,
						Orde = new Orde(){ Id = 2 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 3,
						Orde = new Orde(){ Id = 3 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 4,
						Orde = new Orde(){ Id = 4 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 5,
						Orde = new Orde(){ Id = 5 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 6,
						Orde = new Orde(){ Id = 6 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 7,
						Orde = new Orde(){ Id = 7 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 8,
						Orde = new Orde(){ Id = 8 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 9,
						Orde = new Orde(){ Id = 9 },
						Stemmen = 200
					}
				}
			};
			bool expected = false;
			List<Orde> ordes = new List<Orde>() { new Orde() { Id = 4 }, new Orde() { Id = 5 }, new Orde() { Id = 6 } };

			//Act
			bool? actual = verkiezing.TafelMeerderheid(ordes);

			//Assert
			Assert.AreEqual(expected, actual);
		}

		[TestMethod()]
		public void TafelMeerderheidTest_Negative()
		{
			//Arange
			Verkiezing verkiezing = new Verkiezing()
			{
				Naam = "Nederland",
				Datum = DateTime.Now,
				AantalStoelen = 100,
				Uitslagen = new List<Uitslag>()
				{
					new Uitslag()
					{
						Id = 0,
						Orde = new Orde(){ Id = 0 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 1,
						Orde = new Orde(){ Id = 1 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 2,
						Orde = new Orde(){ Id = 2 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 3,
						Orde = new Orde(){ Id = 3 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 4,
						Orde = new Orde(){ Id = 4 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 5,
						Orde = new Orde(){ Id = 5 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 6,
						Orde = new Orde(){ Id = 6 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 7,
						Orde = new Orde(){ Id = 7 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 8,
						Orde = new Orde(){ Id = 8 },
						Stemmen = 200
					},
					new Uitslag()
					{
						Id = 9,
						Orde = new Orde(){ Id = 9 },
						Stemmen = 200
					}
				}
			};


			try
			{
				//Act
				verkiezing.TafelMeerderheid(new List<Orde>());
				//Assert
				Assert.Fail();
			}
			catch
			{
			}

		}

		[TestMethod()]
		public void GetOpperRidderTestPositive()
		{
			//Arange
			List<Orde> ordes = new List<Orde>()
			{
				new Orde(){ Id = 0, OrdeTrekker = "O"},
				new Orde(){ Id = 1, OrdeTrekker = "1"},
				new Orde(){ Id = 2, OrdeTrekker = "2"},
				new Orde(){ Id = 3, OrdeTrekker = "3"},
				new Orde(){ Id = 4, OrdeTrekker = "4"},
				new Orde(){ Id = 5, OrdeTrekker = "5"},
				new Orde(){ Id = 6, OrdeTrekker = "6"},
				new Orde(){ Id = 7, OrdeTrekker = "7"},
				new Orde(){ Id = 8, OrdeTrekker = "8"},
			};
			Verkiezing verkiezing = new Verkiezing()
			{
				Uitslagen = new List<Uitslag>()
				{
					new Uitslag(){ Orde = ordes[0], Stemmen = 100},
					new Uitslag(){ Orde = ordes[1], Stemmen = 50},
					new Uitslag(){ Orde = ordes[2], Stemmen = 200},
					new Uitslag(){ Orde = ordes[3], Stemmen = 10},
					new Uitslag(){ Orde = ordes[4], Stemmen = 100},
					new Uitslag(){ Orde = ordes[5], Stemmen = 80},
					new Uitslag(){ Orde = ordes[6], Stemmen = 70},
					new Uitslag(){ Orde = ordes[7], Stemmen = 10},
					new Uitslag(){ Orde = ordes[8], Stemmen = 100},
				},
				Coalitie = new Coalitie()
				{
					Ordes = new List<Orde>()
					{
						ordes[0],
						ordes[2],
						ordes[5],
						ordes[8]
					}
				}
			};
			string expected = ordes[2].OrdeTrekker;

			//Act
			string actual = verkiezing.OpperRidder();

			//Assert
			Assert.AreEqual(expected, actual);
		}

		[TestMethod()]
		public void GetOpperRidderTestNegative()
		{
			//Arange
			List<Orde> ordes = new List<Orde>()
			{
				new Orde(){ Id = 0, OrdeTrekker = "O"},
				new Orde(){ Id = 1, OrdeTrekker = "1"},
				new Orde(){ Id = 2, OrdeTrekker = "2"},
				new Orde(){ Id = 3, OrdeTrekker = "3"},
				new Orde(){ Id = 4, OrdeTrekker = "4"},
				new Orde(){ Id = 5, OrdeTrekker = "5"},
				new Orde(){ Id = 6, OrdeTrekker = "6"},
				new Orde(){ Id = 7, OrdeTrekker = "7"},
				new Orde(){ Id = 8, OrdeTrekker = "8"},
			};
			Verkiezing verkiezing = new Verkiezing()
			{
				Uitslagen = new List<Uitslag>()
				{
					new Uitslag(){ Orde = ordes[0], Stemmen = 100},
					new Uitslag(){ Orde = ordes[1], Stemmen = 50},
					new Uitslag(){ Orde = ordes[2], Stemmen = 200},
					new Uitslag(){ Orde = ordes[3], Stemmen = 10},
					new Uitslag(){ Orde = ordes[4], Stemmen = 100},
					new Uitslag(){ Orde = ordes[5], Stemmen = 80},
					new Uitslag(){ Orde = ordes[6], Stemmen = 70},
					new Uitslag(){ Orde = ordes[7], Stemmen = 10},
					new Uitslag(){ Orde = ordes[8], Stemmen = 100},
				},
				Coalitie = new Coalitie()
				{
					Ordes = new List<Orde>()
				}
			};

			try
			{
				//Act 
				 verkiezing.OpperRidder();
				//Assert
				Assert.Fail();
			}
			catch
			{
				
			}
			
		}
	}
}