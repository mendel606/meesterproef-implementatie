﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Factory;

namespace Logic.Tests
{
	[TestClass()]
	public class OrdeTests
	{
		Orde_Manager ordeM = new Orde_Manager(DataClasses.ordeDataModifier());

		[TestMethod()]
		public void OrdesTestPositive()
		{
			//Arange
			
			try
			{
				//Act
				List<Orde> ordes = ordeM.Ordes();
			}
			catch
			{
				//Assert
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void AddTestPositive()
		{
			try
			{
				//Arange
				Orde orde = new Orde()
				{
					Afkorting = "VDR",
					Naam = "Vaarende Drijvende Ridders",
					OrdeTrekker = "Jan"
				};


				//Act
				ordeM.VoegToe(orde);

				//Assert
				Assert.IsNotNull(ordeM.Ordes().Find(O => O.Naam == orde.Naam));
			}
			catch
			{
				Assert.Fail();

			}
		}

		[TestMethod()]
		public void AddTestNegativeNo_Afkorting()
		{
			try
			{
				//Arange
				Orde orde = new Orde()
				{
					Naam = "Vaarende Drijvende Ridders2",
					OrdeTrekker = "Jan"
				};


				try
				{
					//Act
					ordeM.VoegToe(orde);
				}
				catch
				{
					//Assert
					Assert.IsNull(ordeM.Ordes().Find(O => O.Naam == orde.Naam));
				}
			}
			catch
			{
				Assert.Fail();

			}
		}

		[TestMethod()]
		public void AddTestNegativeNo_Naam()
		{
			try
			{
				//Arange
				Orde orde = new Orde()
				{
					Afkorting = "VDR2",
					OrdeTrekker = "Jan"
				};


				try
				{
					//Act
					ordeM.VoegToe(orde);
				}
				catch
				{
					//Assert
					Assert.IsNull(ordeM.Ordes().Find(O => O.Afkorting == orde.Afkorting));
				}
			}
			catch
			{
				Assert.Fail();

			}
		}

		[TestMethod()]
		public void AddTestNegativeNo_OrdeTrekker()
		{
			try
			{
				//Arange
				Orde orde = new Orde()
				{
					Naam = "Vaarende Drijvende Ridders3",
					Afkorting = "VRD3"
				};


				try
				{
					//Act
					ordeM.VoegToe(orde);
				}
				catch
				{
					//Assert
					Assert.IsNull(ordeM.Ordes().Find(O => O.Naam == orde.Naam));
				}
			}
			catch
			{
				Assert.Fail();

			}
		}

		[TestMethod()]
		public void UpdateTestPositive()
		{
			try
			{
				//Arange
				Orde orde = new Orde()
				{
					Afkorting = "VDR4",
					Naam = "Vaarende Drijvende Ridders4",
					OrdeTrekker = "Jan"
				};
				ordeM.VoegToe(orde);
				orde = ordeM.Ordes().Find(O => O.Naam == orde.Naam);
				orde.OrdeTrekker = "Jan3";

				//Act
				ordeM.Verander(orde);

				//Assert
				Assert.AreEqual(ordeM.Orde(orde.Id).OrdeTrekker, orde.OrdeTrekker);
			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void UpdateTestNegativeNo_Afkorting()
		{
			try
			{
				//Arange
				Orde orde = ordeM.Ordes().Find(O => O.Naam == "Vaarende Drijvende Ridders");
				orde.Afkorting = "";


				try
				{
					//Act
					ordeM.Verander(orde);
				}
				catch
				{
					//Assert
					Assert.AreNotEqual(ordeM.Ordes().Find(O => O.Naam == orde.Naam).Afkorting, orde.Afkorting);
				}
			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void UpdateTestNegativeNo_Naam()
		{
			try
			{
				//Arange
				Orde orde = ordeM.Ordes().Find(O => O.Naam == "Vaarende Drijvende Ridders");
				orde.Naam = "";


				try
				{
					//Act
					ordeM.Verander(orde);
				}
				catch
				{
					//Assert
					Assert.AreNotEqual(ordeM.Ordes().Find(O => O.Afkorting == orde.Afkorting).Naam, orde.Naam);
				}
			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void UpdateTestNegativeNo_OrdeTrekker()
		{
			try
			{
				//Arange
				Orde orde = ordeM.Ordes().Find(O => O.Naam == "Vaarende Drijvende Ridders");
				orde.OrdeTrekker = "";


				try
				{
					//Act
					ordeM.Verander(orde);
				}
				catch
				{
					//Assert
					Assert.AreNotEqual(ordeM.Ordes().Find(O => O.Naam == orde.Naam).OrdeTrekker, orde.OrdeTrekker);
				}
			}
			catch
			{
				Assert.Fail();
			}
		}
	}

	
}