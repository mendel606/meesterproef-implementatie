﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Factory;

namespace Logic.Tests
{
	[TestClass()]
	public class UitslagTests
	{
		Uitslag_Manager uitslagM = new Uitslag_Manager(DataClasses.uitslagDataModifier());
		Orde_Manager ordeM = new Orde_Manager(DataClasses.ordeDataModifier());
		Verkiezing_Manager verkiezingM = new Verkiezing_Manager(DataClasses.verkiezingDataModifier());

		[TestMethod()]
		public void AddTestPositive()
		{
			//Arange
			Verkiezing verkiezing = new Verkiezing()
			{
				Naam = "Nederland",
				Datum = DateTime.Now,
				AantalStoelen = 100
			};
			Orde orde = new Orde()
			{
				Afkorting = "VVR",
				Naam = "Volle Vatsige Ridders",
				OrdeTrekker = "Theobold Zwaardmans"
			};
			Uitslag uitslag = new Uitslag()
			{
				Orde = orde,
				Stemmen = 22
			};
			ordeM.VoegToe(orde);
			verkiezingM.VoegToe(verkiezing);

			orde = ordeM.Ordes().Find(o => o.Naam == orde.Naam);
			verkiezing = verkiezingM.Verkiezingen().Find(v => v.Naam == verkiezing.Naam);
			uitslag.Orde = orde;

			//Act
			uitslagM.VoegToe(uitslag, verkiezing.Id);

			//Assert
			Assert.IsNotNull(uitslagM.Uitslagen(verkiezing.Id).Find(u => u.Stemmen == uitslag.Stemmen));
		}

		[TestMethod()]
		public void AddTestNegativeNo_Orde()
		{
			try
			{
				//Arange
				Verkiezing verkiezing = new Verkiezing()
				{
					Naam = "Nederland",
					Datum = DateTime.Now,
					AantalStoelen = 100
				};
				Uitslag uitslag = new Uitslag()
				{
					Stemmen = 21
				};
				verkiezingM.VoegToe(verkiezing);

				verkiezing = verkiezingM.Verkiezingen().Find(v => v.Naam == verkiezing.Naam);

				//Act
				uitslagM.VoegToe(uitslag, verkiezing.Id);

				//Assert
				Assert.IsNull(uitslagM.Uitslagen(verkiezing.Id).Find(u => u.Stemmen == uitslag.Stemmen));
			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void AddTestNegativeNo_Stemmen()
		{
			try
			{
				//Arange
				Verkiezing verkiezing = new Verkiezing()
				{
					Naam = "Nederland",
					Datum = DateTime.Now,
					AantalStoelen = 100
				};
				Orde orde = new Orde()
				{
					Afkorting = "VVR",
					Naam = "Volle Vatsige Ridders",
					OrdeTrekker = "Theobold Zwaardmans"
				};
				Uitslag uitslag = new Uitslag()
				{
					Orde = orde
				};
				ordeM.VoegToe(orde);
				verkiezingM.VoegToe(verkiezing);

				orde = ordeM.Ordes().Find(o => o.Naam == orde.Naam);
				verkiezing = verkiezingM.Verkiezingen().Find(v => v.Naam == verkiezing.Naam);
				uitslag.Orde = orde;

				//Act
				uitslagM.VoegToe(uitslag, verkiezing.Id);

				//Assert
				Assert.IsNull(uitslagM.Uitslagen(verkiezing.Id).Find(u => u.Orde == uitslag.Orde));
			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void UpdateTestPositive()
		{
			try
			{
				//Arange
				Verkiezing verkiezing = new Verkiezing() { Naam = "UpdateTest", AantalStoelen = 10, Datum = DateTime.Today };
				verkiezingM.VoegToe(verkiezing);
				verkiezing = verkiezingM.Verkiezingen().Find(V => V.Naam == verkiezing.Naam);
				Orde orde = new Orde { Naam = "nogwa", Afkorting = "NW", OrdeTrekker = "Peter" };
				ordeM.VoegToe(orde);
				orde = ordeM.Ordes().Find(O => O.Naam == orde.Naam);
				Uitslag uitslag = new Uitslag() { Orde = orde, Stemmen = 10 };
				uitslagM.VoegToe(uitslag, verkiezing.Id);
				uitslag = uitslagM.Uitslagen(verkiezing.Id).Find(U => U.Orde.Naam == uitslag.Orde.Naam);
				uitslag.Stemmen++;

				//Act
				uitslagM.Verander(uitslag);

				//Assert
				Assert.AreEqual(uitslagM.Uitslag(uitslag.Id).Stemmen, uitslag.Stemmen);

			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void UpdateTestNegativeNo_Orde()
		{
			try
			{
				//Arange
				Verkiezing verkiezing = new Verkiezing() { Naam = "UpdateTest", AantalStoelen = 10, Datum = DateTime.Today };
				verkiezingM.VoegToe(verkiezing);
				verkiezing = verkiezingM.Verkiezingen().Find(V => V.Naam == verkiezing.Naam);
				Orde orde = new Orde { Naam = "nogwa", Afkorting = "NW", OrdeTrekker = "Peter" };
				ordeM.VoegToe(orde);
				orde = ordeM.Ordes().Find(O => O.Naam == orde.Naam);
				Uitslag uitslag = new Uitslag() { Orde = orde, Stemmen = 10 };
				uitslagM.VoegToe(uitslag, verkiezing.Id);
				uitslag = uitslagM.Uitslagen(verkiezing.Id).Find(U => U.Orde.Naam == uitslag.Orde.Naam);
				uitslag.Orde = null;

				
				try
				{
					//Act
					uitslagM.Verander(uitslag);
				}
				catch
				{
					//Assert
					Assert.IsNotNull(uitslagM.Uitslag(uitslag.Id).Orde);
				}
			}
			catch
			{
				Assert.Fail();
			}
		}

		[TestMethod()]
		public void UpdateTestNegativeNo_Stemmen()
		{
			try
			{
				//Arange
				Verkiezing verkiezing = new Verkiezing() { Naam = "UpdateTest", AantalStoelen = 10, Datum = DateTime.Today };
				verkiezingM.VoegToe(verkiezing);
				verkiezing = verkiezingM.Verkiezingen().Find(V => V.Naam == verkiezing.Naam);
				Orde orde = new Orde { Naam = "nogwa", Afkorting = "NW", OrdeTrekker = "Peter" };
				ordeM.VoegToe(orde);
				orde = ordeM.Ordes().Find(O => O.Naam == orde.Naam);
				Uitslag uitslag = new Uitslag() { Orde = orde, Stemmen = 10 };
				uitslagM.VoegToe(uitslag, verkiezing.Id);
				uitslag = uitslagM.Uitslagen(verkiezing.Id).Find(U => U.Orde.Naam == uitslag.Orde.Naam);
				uitslag.Stemmen = 0;

				try
				{
					//Act
					uitslagM.Verander(uitslag);
				}
				catch
				{
					//Assert
					Assert.AreNotEqual(uitslagM.Uitslag(uitslag.Id).Stemmen, uitslag.Stemmen);
				}
			}
			catch
			{
				Assert.Fail();
			}
		}
	}
}