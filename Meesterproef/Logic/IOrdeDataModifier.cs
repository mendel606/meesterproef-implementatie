﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public interface IOrdeDataModifier
	{
		void VoegToe(Orde orde);

		void Verander(Orde orde);

		void Verwijder(int ordeId);

		List<Orde> Ordes();

		Orde Orde(int ordeId);
	}
}
