﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public interface ICoalitieDataModifier
	{
		void VoegToe(Coalitie coalitie);

		void Verander(Coalitie coalitie);

		void Verwijder(int coalitieId);

		List<Coalitie> Coalities();

		Coalitie Coalitie(int coalitieId);
	}
}
