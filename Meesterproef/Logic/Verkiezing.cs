﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Verkiezing
	{
		public int Id { get; set; }
		public string Naam { get; set; }
		public DateTime Datum { get; set; }
		public int AantalStoelen { get; set; }
		public List<Uitslag> Uitslagen { get; set; }
		public Coalitie Coalitie { get; set; }

		public Verkiezing()
		{
			Uitslagen = new List<Uitslag>();
		}
		
		public void Export()
		{
			string fileName = @"C:\Mendel\" + Naam + " " + Datum.ToShortDateString() + ".txt";

			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}

			// Create a new file     
			using (FileStream fs = File.Create(fileName))
			{
				// Add some text to file    
				Byte[] title = new UTF8Encoding(true).GetBytes(Naam + " " + Datum.ToShortDateString());
				fs.Write(title, 0, title.Length);
				byte[] author = new UTF8Encoding(true).GetBytes("Mendel Hendriks");
				fs.Write(author, 0, author.Length);
			}

			using (StreamWriter writetext = new StreamWriter(fileName))
			{
				writetext.WriteLine("Coalitie: " + Coalitie.Naam);
				writetext.WriteLine("Orde	      Stemmen	      Percentage    Stoelen");
				writetext.WriteLine();

				foreach (Uitslag uitslag in Uitslagen)
				{
					if (Coalitie.Ordes.Find(O => O.Id == uitslag.Orde.Id) != null)
					{
						var percentage = Math.Round(((double)uitslag.Stemmen / (double)TotaalStemmen()) * 100, 2, MidpointRounding.AwayFromZero);
						writetext.WriteLine(uitslag.Orde.Afkorting + "	      " + uitslag.Stemmen + "	      " + percentage + "	    " + uitslag.Orde.Zetels(this));
					}
				}
				writetext.WriteLine("Totaal aantal zetels van coalitie: " + Zetels(Coalitie.Ordes));
			}
		}

		public bool? TafelMeerderheid(List<Orde> ordes)
		{
			if(ordes.Count < 1)
			{
				return null;
			}
			if (Zetels(ordes) > ((double)AantalStoelen / 2))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public int Zetels(List<Orde> ordes)
		{
			int zetels = 0;
			foreach (Orde orde in ordes)
			{
				zetels += orde.Zetels(this);
			}
			return zetels;
		}

		public string OpperRidder()
		{
			foreach(Uitslag uitslag in Uitslagen.OrderByDescending(O => O.Stemmen))
			{
				if(Coalitie.Ordes.Find(O => O.Id == uitslag.Orde.Id) != null)
				{
					return uitslag.Orde.OrdeTrekker;
				}
			}
			return "";
		}

		private int TotaalStemmen()
		{
			int stemmen = 0;
			foreach (Uitslag uitslag in Uitslagen)
			{
				stemmen += uitslag.Stemmen;
			}
			return stemmen;
		}
	}
}