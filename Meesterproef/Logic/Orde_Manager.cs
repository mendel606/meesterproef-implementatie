﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Orde_Manager
	{

		IOrdeDataModifier OrdeData;

		public Orde_Manager(IOrdeDataModifier dataModifier)
		{
			OrdeData = dataModifier;
		}

		public void VoegToe(Orde orde)
		{
			OrdeData.VoegToe(orde);
		}

		public void Verander(Orde orde)
		{
			OrdeData.Verander(orde);
		}

		public void Delete(int ordeId)
		{
			OrdeData.Verwijder(ordeId);
		}

		public List<Orde> Ordes()
		{
			return OrdeData.Ordes();
		}

		public Orde Orde(int ordeId)
		{
			return OrdeData.Orde(ordeId);
		}
	}
}
