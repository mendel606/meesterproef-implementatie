﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Coalitie_Manager
	{
		ICoalitieDataModifier CoalitieData;

		public Coalitie_Manager(ICoalitieDataModifier dataModifier)
		{
			CoalitieData = dataModifier;
		}

		public void VoegToe(Coalitie coalitie)
		{
			CoalitieData.VoegToe(coalitie);
		}

		public void Verander(Coalitie coalitie)
		{
			CoalitieData.Verander(coalitie);
		}

		public void Delete(int coalitieId)
		{
			CoalitieData.Verwijder(coalitieId);
		}

		public List<Coalitie> Coalities()
		{
			return CoalitieData.Coalities();
		}

		public Coalitie Coalitie(int coalitieId)
		{
			return CoalitieData.Coalitie(coalitieId);
		}
	}
}
