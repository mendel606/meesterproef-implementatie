﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Uitslag_Manager
	{
		IUitslagDataModifier UitslagData;

		public Uitslag_Manager(IUitslagDataModifier dataModifier)
		{
			UitslagData = dataModifier;
		}

		public void VoegToe(Uitslag Uitslag, int verkiezingId)
		{
			try
			{
				UitslagData.VoegToe(Uitslag, verkiezingId);
			}
			catch
			{

			}
		}

		public void Verander(Uitslag Uitslag)
		{
			UitslagData.Verander(Uitslag);
		}

		public void Delete(int UitslagId)
		{
			UitslagData.Verwijder(UitslagId);
		}

		public List<Uitslag> Uitslagen(int verkiezingId)
		{
			return UitslagData.Uitslagen(verkiezingId);
		}

		public Uitslag Uitslag(int UitslagId)
		{
			return UitslagData.Uitslag(UitslagId);
		}
	}
}
