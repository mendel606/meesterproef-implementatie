﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Verkiezing_Manager
	{
		IVerkiezingDataModifier VerkiezingData;
		public Verkiezing_Manager(IVerkiezingDataModifier dataModifier)
		{
			VerkiezingData = dataModifier;
		}

		public void VoegToe(Verkiezing verkiezing)
		{
			try
			{
				VerkiezingData.VoegToe(verkiezing);
			}
			catch
			{

			}
		}

		public void Verander(Verkiezing verkiezing)
		{
			VerkiezingData.Verander(verkiezing);
		}

		public void Delete(int verkiezingId)
		{
			VerkiezingData.Verwijder(verkiezingId);
		}

		public List<Verkiezing> Verkiezingen()
		{
			return VerkiezingData.Verkiezingen();
		}

		public Verkiezing Verkiezing(int verkiezingId)
		{
			return VerkiezingData.Verkiezing(verkiezingId);
		}
	}
}
