﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Uitslag
	{
		public int Id { get; set; }
		public Orde Orde { get; set; }
		public int Stemmen { get; set; }

		public Uitslag()
		{
			
		}
	}
}