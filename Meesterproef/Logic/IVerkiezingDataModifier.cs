﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public interface IVerkiezingDataModifier
	{
		void VoegToe(Verkiezing verkiezing);

		void Verander(Verkiezing verkiezing);

		void Verwijder(int verkiezingId);

		List<Verkiezing> Verkiezingen();

		Verkiezing Verkiezing(int verkiezingId);
	}
}
