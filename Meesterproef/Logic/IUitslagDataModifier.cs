﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public interface IUitslagDataModifier
	{
		void VoegToe(Uitslag uitslag, int verkiezingId);

		void Verander(Uitslag uitslag);

		void Verwijder(int uitslagId);

		List<Uitslag> Uitslagen(int verkiezingId);

		Uitslag Uitslag(int uitslagId);
	}
}
