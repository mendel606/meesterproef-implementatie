﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Orde
	{
		public int Id { get; set; }
		public string Afkorting { get; set; }
		public string Naam { get; set; }
		public string OrdeTrekker { get; set; }

		public Orde()
		{

		}


		//Bereken het aantal zelels voor deze Orde

		public int Zetels(Verkiezing verkiezing)
		{
			double AantalStoelen = verkiezing.AantalStoelen;
			double AlleStemmen = 0;
			foreach (Uitslag uitslag in verkiezing.Uitslagen)
			{
				AlleStemmen += uitslag.Stemmen;
			}
			double Stemmen = verkiezing.Uitslagen.Find(O => O.Id == Id).Stemmen;
			var zetels = Math.Round(((Stemmen / AlleStemmen) * AantalStoelen), 0, MidpointRounding.AwayFromZero);
			return (int)zetels;
		}
		
	}
}