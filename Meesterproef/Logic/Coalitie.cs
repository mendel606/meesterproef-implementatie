﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Coalitie
	{
		public int Id { get; set; }
		public string Naam { get; set; }
		public string OpperRidder { get; set; }
		public List<Orde> Ordes { get; set; }

		public Coalitie()
		{
			Ordes = new List<Orde>();
		}
	}
}